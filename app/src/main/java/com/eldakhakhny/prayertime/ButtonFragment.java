package com.eldakhakhny.prayertime;

//try whne there is noview  meaning remoteview not initailized 
// try registering the listenr in on create maybe thats what doing the error
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class ButtonFragment extends Fragment {
	Button doneB;
	public static String TAG="ButtonFragment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view=inflater.inflate(R.layout.button_fragment_layout, null);
		doneB= (Button)view.findViewById(R.id.b_done);
		doneB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				getActivity().finish();
			}
		}); 
		return view; 
		}

}