package com.eldakhakhny.prayertime;

public class Constants {
	static final String ACTION_SILENT="silent_the_mobile";
	static final String ACTION_UPDATE="android.appwidget.action.APPWIDGET_UPDATE";
	
	//the folowing actions must be mentioned in the manifest 
	static final String ACTION_MY_UPDATE_SIELENT="com.eldakhakhny.prayertime.intent.action.UPDATE_WIDGET_SIELENT";
	static final String ACTION_MY_UPDATE_GENERAL="com.eldakhakhny.prayertime.intent.action.UPDATE_WIDGET_GENRRAL";
	//action pick time method is the action that switch betwen defult delta time and picking time 
	static final String ACTION_MY_PICK_TIME_METHOD="com.eldakhakhny.prayertime.intent.action.PICK_TIME";

	//static final long DELTA_TIME=24*60*60*1000;
	//static final long DELTA_TIME=4*1000;
	static final long MILLISEC_TO_MIN_CONV=60*1000;
	static final String PRAYER_PERIOD_DEFULT="10";
	
	//beep
	static final int BEEP_VOLUME=50;
	static final int BEEP_DURATION=100;
	
	//delta time picker
	static final int MAX_MINIUTS=210;
	static final int MIN_MINIUTS=1;
	
	//appwidget id
	static final String APPWIDGET_ID="com.eldakhakhny.prayertime.app.id";
}

