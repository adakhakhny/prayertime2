package com.eldakhakhny.prayertime;
//passing appwidget context  and using it here

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.util.GregorianCalendar;

public class DeltaTimePickerFragment extends DialogFragment {
	Button setB,cancelB;
	NumberPicker detaTimeNP;
	String TAG="DeltaTimePickerFragment";
	@Override
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view=inflater.inflate(R.layout.delta_time_dialog_layout, null);
		setCancelable(false);
		getDialog().setTitle(getResources().getString(R.string.delta_time_dialog_layout_title));
		
		detaTimeNP=(NumberPicker)view.findViewById(R.id.np_delta_time);
		detaTimeNP.setMaxValue(Constants.MAX_MINIUTS);
		detaTimeNP.setMinValue(Constants.MIN_MINIUTS);
		detaTimeNP.setValue(Integer.parseInt(Constants.PRAYER_PERIOD_DEFULT));
		cancelB=(Button) view.findViewById(R.id.b_cancel);
		setB=(Button) view.findViewById(R.id.b_set);
		//listeners 
		
		cancelB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Log.v(TAG,"cancel was clicked");
				//getView().setVisibility(View.GONE);
				dismiss();
				//getView().setVisibility(View.INVISIBLE);
				//getActivity().finish();				
			}
		});
		setB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Log.v(TAG,"set was clicked");
				setDeltaTime();
				//updateSilent();
				sendUpdateBroadcast();
				//scheduleUpdateGeneral();
				//getView().setVisibility(View.GONE);
				dismiss();
				
				//getView().setVisibility(View.INVISIBLE);
				//getActivity().finish();
				
			}
		});
		return view;
		
	}
	
	private void sendUpdateBroadcast() {
		Intent intent = new Intent(Constants.ACTION_MY_UPDATE_SIELENT);
		getActivity().sendBroadcast(intent);
	}

	
	private void updateSilent() {
		Log.v(TAG, "updateSilent was called");
		final AudioManager mode = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
		mode.setRingerMode(AudioManager.RINGER_MODE_SILENT);
	}
	private void scheduleUpdateGeneral() {
		Log.v(TAG, "scheduleUpdateGeneral was called");
		Long time = new GregorianCalendar().getTimeInMillis()+ detaTimeNP.getValue()*Constants.MILLISEC_TO_MIN_CONV;
		Intent intentAlarm =new Intent(Constants.ACTION_MY_UPDATE_GENERAL);
		AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP,time, PendingIntent.getBroadcast( getActivity(),1,  intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
		Toast.makeText( getActivity(), " have saa 5ashe3a alarm will be in "+detaTimeNP.getValue(),Toast.LENGTH_LONG).show();
		
	}
	public void onDismiss(android.content.DialogInterface dialog) {
		Log.v(TAG, "ondismiss was called");
		super.onDismiss(dialog);
		((Callback)getActivity()).dismissActivityCallback();
	};
	protected void setDeltaTime() {
		SharedPreferences sp =PreferenceManager.getDefaultSharedPreferences(getActivity());
		Editor e=sp.edit();
		e.putString("prayer_period_dialog", detaTimeNP.getValue()+"");
		e.commit();
	}
	@Override
	public void onPause() {

		Log.v(TAG, "onPause was called");
		super.onPause();
	}
	public interface Callback {

		public void dismissActivityCallback();

	}

}
