package com.eldakhakhny.prayertime;
//passing appwidget context  and using it here

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.NumberPicker;

public class HowToDialogFragment extends DialogFragment{
	Button okB;
	NumberPicker detaTimeNP;
	String TAG="HowToDialogFragment";
	SharedPreferences sp;
	@Override
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		sp=PreferenceManager.getDefaultSharedPreferences(getActivity());
		View view=inflater.inflate(R.layout.how_to_layout, null);
         setCancelable(true);
         getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
         okB= (Button) view.findViewById(R.id.b_how_to_ok_button);
         okB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("first_time", false).commit();
				dismiss();
			}
		});
		return view;
		
	}
	
	
	
	public void onDismiss(android.content.DialogInterface dialog) {
		Log.v(TAG, "ondismiss was called");

	};
	
	@Override
	public void onPause() {

		Log.v(TAG, "onPause was called");
		super.onPause();
	}}