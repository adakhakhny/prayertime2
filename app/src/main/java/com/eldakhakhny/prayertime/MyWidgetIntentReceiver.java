package com.eldakhakhny.prayertime;

import java.util.GregorianCalendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

public class MyWidgetIntentReceiver extends BroadcastReceiver {
	public static String  TAG ="com.eldakhakhny.prayertime;";
	SharedPreferences sp;
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "context.out "+context.toString());
		sp= PreferenceManager.getDefaultSharedPreferences(context);
		if (intent.getAction().equals(Constants.ACTION_MY_UPDATE_SIELENT)){
			Log.i(TAG, "before update silent");
			updateSilent(context);
			Log.i(TAG, "after update silent");
		}else if(intent.getAction().equals(Constants.ACTION_MY_UPDATE_GENERAL)){
			Log.i(TAG, " update to general");
			updateGeneral(context);
		}else if(intent.getAction().equals(Constants.ACTION_MY_PICK_TIME_METHOD)){
			Log.i(TAG, " pick method ");
			pickTimeMethod(context);
		}
		
		
	}

	private void pickTimeMethod(Context con) {
		Intent intent;
		if (sp.getBoolean("show_pick_time_dialog", false)){
			Log.v(TAG,"show_pick_time_dialog  true");
			intent = new Intent(con, TimePickerActivity.class);
			// Old activities shouldn't be in the history stack
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			con.startActivity(intent);
		}
		else{
			Log.v(TAG,"show_pick_time_dialog  false");
			updateSilent(con);
		}
		
	}

	private void updateGeneral(Context context) {
		//turn mobile silent here
		Log.i(TAG, "just turned general");
		final AudioManager mode = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		mode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		Toast.makeText(context, "your phone is back on ",Toast.LENGTH_LONG).show();
		//beep sound
		if (sp.getBoolean("beep", false)){
			
			ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, Constants.BEEP_VOLUME);
			toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD,Constants.BEEP_DURATION); 
			Log.i(TAG, "beep");
		}
	}

	private void updateSilent(Context context) {
		//turn mobile silent here
		Log.i(TAG, "just turned silent");
		//SilentToNomal and NormalToSilent device Programatically
		final AudioManager mode = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		//Silent Mode Programatically
		mode.setRingerMode(AudioManager.RINGER_MODE_SILENT);

		//Normal Mode Programatically
		//mode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(),R.layout.appwidget_layout);
		// re-registering for click listener
		remoteViews.setOnClickPendingIntent(R.id.ib_update,PrayerTimeAppWidgetProvider.buildButtonPendingIntent(context));

		PrayerTimeAppWidgetProvider.pushWidgetUpdate(context.getApplicationContext(),
				remoteViews);
		// register turn to general task here
		scheduleTurnToGeneralTask(context);
		
	}

	private void scheduleTurnToGeneralTask(Context context) {
		Log.i(TAG, "scheduleTurnToGeneralTask");
		long deltaMin= Long.parseLong(Constants.PRAYER_PERIOD_DEFULT);
		if (sp.getBoolean("show_pick_time_dialog", false)){
			//dialog
			deltaMin= Long.parseLong(sp.getString("prayer_period_dialog", Constants.PRAYER_PERIOD_DEFULT));
		}else{
			//default
			deltaMin= Long.parseLong(sp.getString("prayer_period", Constants.PRAYER_PERIOD_DEFULT));
		}
		
		Long time = new GregorianCalendar().getTimeInMillis()+deltaMin*Constants.MILLISEC_TO_MIN_CONV;
		Intent intentAlarm =new Intent(Constants.ACTION_MY_UPDATE_GENERAL);
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP,time, PendingIntent.getBroadcast(context,1,  intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT));
		if(deltaMin==1){
			Toast.makeText(context, context.getResources().getString(R.string.toasts_your_phone_will_be_back_one_minute),Toast.LENGTH_SHORT ).show();
			
			}
		else{
			Toast.makeText(context, context.getResources().getString(R.string.toasts_your_phone_will_be_back)+" "+deltaMin+" "+context.getResources().getString(R.string.toasts_minutes),Toast.LENGTH_SHORT).show();
			
		}
		Toast.makeText(context, context.getResources().getString(R.string.toasts_pray_in_peace),Toast.LENGTH_LONG).show();
	}

}
