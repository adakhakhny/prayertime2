package com.eldakhakhny.prayertime;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Map;

public class PrayerTimeAppWidgetProvider extends AppWidgetProvider {
	static SharedPreferences sp;
	public static String TAG="PrayerTimeAppWidgetProvider";
	Context mContext;
	RemoteViews remoteViews=null ;
	int[] appWidgetIdsTemp={0};
	
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.v(TAG,"onEnabled ");
		sp= PreferenceManager.getDefaultSharedPreferences(context);
	}
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.v(TAG,"on receive ");
		if (intent.hasExtra(Constants.APPWIDGET_ID)) {
			this.onUpdate(context, AppWidgetManager.getInstance(context),appWidgetIdsTemp);
		}else{
			super.onReceive(context, intent);
		}
	}
	
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,int[] appWidgetIds) {
		Log.v(TAG,"onUpdate ");
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		sp= PreferenceManager.getDefaultSharedPreferences(context);
		remoteViews = new RemoteViews(context.getPackageName(),R.layout.appwidget_layout);
		remoteViews.setOnClickPendingIntent(R.id.ib_update,buildButtonPendingIntent(context));	
		pushWidgetUpdate(context, remoteViews);
		
	}
	public static PendingIntent buildButtonPendingIntent(Context context) {
		// initiate widget update request
		Log.v(TAG,"buildButtonPendingIntent ");
		Intent intent;
		intent = new Intent(Constants.ACTION_MY_PICK_TIME_METHOD);
		return PendingIntent.getBroadcast(context, 0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
	
	}
	
	
	public static void pushWidgetUpdate(Context context, RemoteViews remoteViews) {
		Log.v(TAG,"pushWidgetUpdate ");
		ComponentName myWidget = new ComponentName(context,PrayerTimeAppWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		manager.updateAppWidget(myWidget, remoteViews);
	}
	public void mapkeys(){
		Map<String,?> keys = sp.getAll();

		for(Map.Entry<String,?> entry : keys.entrySet()){
		            Log.d("map values",entry.getKey() + ": " + 
		                                   entry.getValue().toString());            
		 }
	}
}
