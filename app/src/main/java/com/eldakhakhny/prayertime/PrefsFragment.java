package com.eldakhakhny.prayertime;

//try whne there is noview  meaning remoteview not initailized 
// try registering the listenr in on create maybe thats what doing the error

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.appwidget.AppWidgetManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.widget.RemoteViews;
import android.widget.Toast;

public class PrefsFragment extends PreferenceFragment {
	 SharedPreferences sp;
	 Boolean firstTime=false;
	 //Dialog builder ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);

                // Load the preferences from an XML resource
                addPreferencesFromResource(R.xml.prefs_home_screen);
                Preference preference = (Preference) findPreference("info");
                sp= PreferenceManager.getDefaultSharedPreferences(getActivity());
              if(sp.getBoolean("first_time", true)){
                showDialogFragment();
              }
//                	builder= new Dialog(getActivity());
   
//                    // Create the AlertDialog object and return it
//                    
//                    builder.show();
//                }
                preference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                 
                    public boolean onPreferenceClick(Preference preference) {
                    	Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                    	Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    	try {
                    	  startActivity(goToMarket);
                    	} 
                    	catch (ActivityNotFoundException e) {
                    	Log.v("oppa","catch");
                    	  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
                    	}
                        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.envyandroid.com"));
                        //startActivity(intent);
						return true;
                    }
                    
                });
                Preference preference2 = (Preference) findPreference("prayer_period");
                
                preference2.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
					
					@Override
					public boolean onPreferenceChange(Preference preference, Object newValue) {
						Log.v("PrefsFragment", "prayer period change");
						// new value should be a nummber notempty and between 1 and max
						
						String newVal=String.valueOf(newValue);
						long newValLong=0;
						try {
							newValLong=Long.parseLong(newVal);
							Log.v("pref 2", "parse ok");
						} catch (Exception e) {
							Log.v("pref 2", "failed to parse");
							return false;
						}
						if (newValLong>0&&newValLong<=Constants.MAX_MINIUTS){
							Log.v("pref 2", "in limit"+newValLong);
							Log.v("pref 2", ""+newValLong);
							Toast.makeText(getActivity(), getActivity().getString(R.string.prefs_home_screen_saved)+" "
							+getActivity().getString(R.string.prefs_home_screen_current_value_is)
							+" "+newValLong, Toast.LENGTH_LONG).show();
							return true;
						}
						else if (newValLong>=Constants.MAX_MINIUTS) {
							//make it max value and save or do not save
							Toast.makeText(getActivity(), getActivity().getString(R.string.prefs_home_screen_exeed_max)+" "
									, Toast.LENGTH_LONG).show();
							Log.v("pref 2", "exedding limit"+newValLong);
							return false;
						}
                		else if (newValLong==0) {
                			Toast.makeText(getActivity(), getActivity().getString(R.string.prefs_home_screen_zero)+" "
									, Toast.LENGTH_LONG).show();
							Log.v("pref 2", "exedding limit"+newValLong);
							Log.v("pref 2", "new value is zero"+newValLong);
							return false;
                		}
                		else if (newValLong<0) {
                			Toast.makeText(getActivity(), getActivity().getString(R.string.prefs_home_screen_less_than_min)+" "
									, Toast.LENGTH_LONG).show();
                			Log.v("pref 2", "new value is negative"+newValLong);
							return false;
                		}
                		else{
                			return false;
                		}
						
						
						
				
					}
			 
                    
                });
                Preference preference3 = (Preference) findPreference("how_to_use");
                
                preference3.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					
					@Override
					public boolean onPreferenceClick(Preference arg0) {
						showDialogFragment();
						return true;
					}
                
              }
				);
                
    
    }
    private void showDialogFragment(){
    	FragmentManager manager= getFragmentManager();
    	HowToDialogFragment frag = new HowToDialogFragment();
    	frag.show(manager, "HowToDialogFragment");
    }

}