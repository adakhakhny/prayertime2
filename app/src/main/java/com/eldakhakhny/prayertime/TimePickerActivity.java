package com.eldakhakhny.prayertime;


import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.NumberPicker;

public class TimePickerActivity extends FragmentActivity  implements DeltaTimePickerFragment.Callback {
	
	TimePickerDialog timePickerDialog;
	String TAG="TimePickerActivity";
	NumberPicker detaTimeNP;

@Override
protected void onCreate(Bundle savedInstanceState) {
	 
	super.onCreate(savedInstanceState);
	Log.v(TAG,"oncreate started");
	showDialogFragment();
	
	
}
private void showDialogFragment(){
	Log.v(TAG,"showDialogFragment started");
	FragmentManager manager= getSupportFragmentManager();
	DeltaTimePickerFragment frag = new DeltaTimePickerFragment();

	frag.show(manager, "DialogFragment");
}

	@Override
	public void onBackPressed() {
		Log.v(TAG,"onBackPressed started");
		super.onBackPressed();
	}

	@Override
	public void dismissActivityCallback() {
		finish();
	}
}
